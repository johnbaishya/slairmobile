import React, { Component } from 'react';
import { NetInfo } from 'react-native';

import { Router, Scene } from 'react-native-router-flux';
import PageOne from './PageOne';
import PageTwo from './PageTwo';
import OfflineNotice from './OfflineNotice';
export default class App extends Component {
  
  render() {
    return (
      <Router hideNavBar= "false">
        <Scene key="root">
          <Scene key="pageOne" component={PageOne} title="PageOne" />
        </Scene>
      </Router>
    )
  }
}