import React, { Component } from 'react';

import { Router, Scene,Stack } from 'react-native-router-flux';
import PageOne from './pages/PageOne';
import login from './pages/login';
import user from './pages/user';
import userInfo from './pages/UserInfo';
export default class App extends Component {
  

  render() {
    return (
      <Router>
        <Stack  key="root">
          <Scene key="pageOne" component={PageOne} title="hello sagar page1"/>
          <Scene key="userInfo" component={userInfo} title="userInfo"/>
          <Scene key="user" component={user} title="user"/>
          <Scene key="login" component={login} initial={true}/>
        </Stack >
      </Router>
    )
  }
}