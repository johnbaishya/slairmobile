import React, { Component } from 'react';
import {Button, Card, CardItem, Body, Left, Right,SwipeRow,Icon} from 'native-base';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Image,
    Alert,
    AsyncStorage,
    FlatList
  } from 'react-native';
  
  const styles = StyleSheet.create({
    details: {
      backgroundColor: '#e9ebec',
    },
    header :{
        backgroundColor: '#dbe0e4'
    },
    headerfont :{
        fontWeight: 'bold',
    },
    red: {
      color: 'red',
    },
    button: {
        width:'100%',
    }
  });
  

  export default class Collapsible extends Component{
      constructor(props){
          super(props);
          this.state={isExpanded:false}
      }
      

      renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: "100%",
              backgroundColor: "#CED0CE",
            }}
          />
        );
      };

      toogleView = () =>{
          this.setState((prevState, prevProps)=>({
              isExpanded:!prevState.isExpanded
          }));
      }  
      Details = (array) =>{
          return(
            <View style = {styles.details}>
                <Card>
                    <CardItem header bordered style = {styles.header}>
                        <Text style = {styles.headerfont}>Sites</Text>
                    </CardItem>
                    <CardItem  style= {styles.details}>
                    <Body>
                    <FlatList
                        data={array.sites}
                        renderItem={({item}) => <Text>{item.name}</Text>}
                        keyExtractor = {item =>item.name}
                        listKey = {item =>item.name}
                        />
                    </Body>
                    </CardItem>
                </Card>
                <Card>
                    <CardItem header bordered style = {styles.header}>
                        <Text style = {styles.headerfont}>Document</Text>
                    </CardItem>
                    <CardItem style= {styles.details}>
                        <Body>
                        <FlatList
                            data={array.docs}
                            renderItem={({item}) =>
                            <View>
                                <Text>File Name: {item.file_name}</Text>
                                <Text>Expiry Date : {item.expiry_date}</Text>
                            </View>
                            }
                            keyExtractor = {item =>item.file_location}
                            listKey = {item =>item.file_location}
                            ItemSeparatorComponent={this.renderSeparator}
                            />
                        </Body>
                    </CardItem>
                </Card>
            </View>
            
          );
      };

          
      
      render(){
          return(
            <View>
            <SwipeRow
                leftOpenValue={75}
                rightOpenValue={-75}
                left={
                    <Button success onPress={() => alert('Add')} >
                    <Icon active name="call" />
                    </Button>
                }
                body={
                <Button block light style={styles.button} transparent
                onPress = {this.toogleView}>
            
                <Card transparent>
                    <CardItem>
                             <Text>{this.props.data.name}</Text>
                    </CardItem>
                </Card>
            
             </Button>
                }
                right={
                <Button danger onPress={() => alert('Info','welcome to the future')}>
                <Icon active name="trash" />
                </Button>
                }
                />
            
        
                {this.state.isExpanded&&this.Details(this.props.data)}
                {/* <Text></Text> */}
              </View>
            
          );
      }
  }


