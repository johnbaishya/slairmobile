import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Image,
    Alert,
    AsyncStorage,
    FlatList
  } from 'react-native';
import {Button, Card, CardItem, Body, Left, Right, Footer, FooterTab, Icon} from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class AppFooter extends Component{
    render(){
        return(
            <Footer>
                <FooterTab>
                    <Button>
                        <Icon name="home" />
                        <Text style={styles.footxt}>Home</Text>
                    </Button>
                    <Button vertical onPress = {() => {Actions.userInfo()}}>
                        <Icon name="list" />
                        <Text style={styles.footxt}>list</Text>
                    </Button>
                    <Button vertical active>
                        <Icon active name="grid"/>
                        <Text style={styles.footxt}>sites</Text>
                    </Button>
                    <Button vertical onPress = {() => {Actions.user()}}>
                        <Icon name="person" />
                        <Text style={styles.footxt}>User Profile</Text>
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}

const styles = StyleSheet.create({
    footxt:{
        color: 'white',
    },
   
  });