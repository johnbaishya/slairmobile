import React, { Component } from 'react';
import { Image,  StyleSheet,AsyncStorage } from 'react-native';
import { createStackNavigator } from 'react-navigation';


import { Container, Header, Content, Card, CardItem, 
  Thumbnail, Text, Button, Icon,
   Left,Right, Body,Title,List,ListItem } from 'native-base';
import { Actions } from 'react-native-router-flux';
import AppFooter from '../../components/appFooter';
export default class user extends Component {
  static navigationOptions = { header: null };
  logout = () => {
    AsyncStorage.clear().then(() => {
      Actions.login();
    }).catch(function(error) {
     alert.alert('Something went Wrong!!!');
    });
  }

  render(){
    
    return(
      
      <Container>
      <Header>
          <Left>
            <Button transparent>
              <Icon name='list' />
            </Button>
          </Left>
          <Body>
            <Title>User Details</Title>
          </Body>
          <Right>
          <Button vertical active style={styles.logoutbtn}
            onPress={() => {this.logout()}}>
                        <Icon name="swap" />
                        <Text>Logout</Text>
                    </Button>
           
          </Right>
        </Header>
        <Content padder>
        <Image style={styles.userimg} source={{uri: 'https://icons-for-free.com/free-icons/png/512/1891016.png'}}/>
           <List>
            <ListItem itemHeader first  style={styles.usern}  >
              <Text>Sagar kc</Text>
            </ListItem>
            <ListItem >
              <Text>Welcome sagar</Text>
            </ListItem>
            <ListItem >
              <Text>Welcome sagar</Text>
            </ListItem>
            <ListItem >
              <Text>Welcome sagar</Text>
            </ListItem>
            <ListItem >
              <Text>Welcome sagar</Text>
            </ListItem>
          
            
        
          </List>
            </Content>
            <AppFooter/>
      </Container>
    );
  }
}


const styles = StyleSheet.create({
  userimg:{
    width:200,
    height:200,
    marginTop:20,
    marginLeft:70,
    justifyContent: 'center'
  },
  usern:{
    justifyContent: 'center'
  },
  logoutbtn: {
    backgroundColor:'red',
  }
});
  