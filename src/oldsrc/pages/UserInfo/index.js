import React, { Component } from 'react';
import { FlatList, ActivityIndicator, AsyncStorage, Animated, TouchableHighlight,
   TouchableOpacity, textSwap, Alert, ListView} from 'react-native';
import { Container, Content, Text,
   Card, Header, Body, Button, Title, 
   CardItem, View, Left,Right,
    Thumbnail, Icon,
    Footer,FooterTab,
     Accordion, List, ListItem} from 'native-base';
     import { createStackNavigator } from 'react-navigation';

import { Actions } from 'react-native-router-flux';
import Collapsible from './../../components/collapsible';
import AppFooter from './../../components/appFooter';


export default class userInfo extends Component {
  constructor(props){
    super(props);
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state ={ 
      isLoading: true,
      auth_token: '',
    }
  }
  static navigationOptions = { header: null };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };

  componentDidMount(){
        AsyncStorage.getItem('UID123', (err, result) => {
          result = JSON.parse(result);
          this.setState({ 
            auth_token: result.access_token,
          });
          fetch('https://my.geniusitsolutions.com.au/api/users?token='+this.state.auth_token)
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              dataSource: responseJson,
              isLoading: false,

            }, function(){
              
            });
        });
      })
      .catch((error) =>{
        console.error(error);
      });
  }

  render(){
    let b=<Card>
    <CardItem>
      <Body>
        <Text>
           //Your text here
        </Text>
      </Body>
    </CardItem>
  </Card>;
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    return(
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='list' />
            </Button>
          </Left>
          <Body>
            <Title>User List</Title>
          </Body>
          <Right>
           
            <Button transparent>
              <Icon name='more' />
            </Button>
          </Right>
        </Header>
        <Content padder>
        {/* <Welcome data = {this.state.dataSource}/> */}
          <FlatList
            data={this.state.dataSource}
            // renderRow={({item}) => <Collapsible data={item}/>}
            renderItem={({ item }) =>(<Collapsible data={item}/>)}
            keyExtractor = {item => item.name}
            ItemSeparatorComponent={this.renderSeparator}
          />
          <Button dark bordered
            onPress= {() => {Actions.pop(); }}>
            <Text>Goto Page 1</Text>
         </Button>
         
        </Content>
        <AppFooter/>
      </Container>
    );
  }
}