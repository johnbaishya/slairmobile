import React, { Component } from 'react';
import { Container, Content, Text, Card, Header, Body, Button, Title, CardItem } from 'native-base';
import { Actions } from 'react-native-router-flux';
export default class PageOne extends Component {
  static navigationOptions = { header: null };

  render(){
    return(
      <Container>
        <Header>
          <Body>
            <Title>Kushal Page</Title>
          </Body>
        </Header>
        <Content padder>
          <Card>
            <CardItem>
              <Body>
                <Text>
                  This is first page One, Press button to goto page two
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Button dark bordered style = {{alignSelf: 'center', margin: 30}}
            onPress= {() => {Actions.pageTwo(); }}>
            <Text>Goto Page 2</Text>
          </Button>
         </Content>
      </Container>
    );
  }
}