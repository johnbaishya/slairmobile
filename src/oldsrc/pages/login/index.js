import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  NetInfo,
  Alert,
  Icon,
  AsyncStorage
} from 'react-native';
import { Actions } from 'react-native-router-flux';
export default class LoginView extends Component {
  
  
  constructor(props) {
    super(props);
    state = {
      username   : '',
      password: '',
      access_token: '',

    }

  }
  static navigationOptions = { header: null };

  
  Login = async () => {
    var fd=new FormData();
    fd.append('email',this.state.username)
    fd.append('password',this.state.password)
    
    fetch('https://my.geniusitsolutions.com.au/api/login', {
          method: 'post',
          body:fd,  
        }).then((response) => response.json())
        .then((res) => {
          let token_data = {
            access_token: res.access_token,
            token_type: res.token_type,
          };
          // You only need to define what will be added or updated
          
          
          AsyncStorage.setItem('UID123', JSON.stringify(token_data), () => {
              AsyncStorage.getItem('UID123', (err, result) => {
                result = JSON.parse(result);
                if(result.access_token){
                  this.setState({ auth_token: result.access_token});
                  Actions.userInfo();
                // console.error(result.access_token);
                }else{
                  alert("Invalid username or password");
                }
                
              });
        
          });
        // store.set('access_token', res.access_token )
        // this.setState({ auth_token: res.access_token });
     })
     
  
  }



  render() {
  
    return (
      <View style={styles.container}>

      <View>
      <Image style={styles.iconuser} source={{uri: 'https://geniusitsolutions.com.au/images/logo.png'}}/>

      </View>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              placeholder="Username"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(username) => this.setState({username})}  />
        </View>
      
        
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(password) => this.setState({password})}/>
        </View>

        <View>
        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]}
            //  onPress={() => {this.Login()}}>
              onPress= {() => {Actions.userInfo(); }}>
  
                   <Text style={styles.loginText}>Login</Text>
        </TouchableHighlight>
       
        
       
        </View>
        <TouchableHighlight style={styles.buttonContainer}>
            <Text>Forgot your password?</Text>
        </TouchableHighlight>
      
      </View>
      
    );
  }
  
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      // borderRadius:30,
      borderBottomWidth: 1,
      width:300,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  iconuser:{
    width:200,
    height:100,
    marginLeft:15,
    marginBottom:20,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
  },
  loginButton: {
    backgroundColor: "#00b5ec",
  },
  loginText: {
    color: 'white',
  },
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    position: 'absolute',
    top: 30
  },
  offlineText: { color: '#fff' }
});
 