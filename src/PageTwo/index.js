import React, { Component } from 'react';
import { Container,
   Content,
   Text,
   Left,
   Right,
    Card, Icon,Header, Body, Button, Title, CardItem,View, Footer,FooterTab} from 'native-base';
import { FlatList, ActivityIndicator  } from 'react-native';

import { Actions } from 'react-native-router-flux';
export default class pageTwo extends Component {
  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  componentDidMount(){
    return fetch('https://my.geniusitsolutions.com.au/api/users')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }



  render(){
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    return(
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Header</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon name='search' />
            </Button>
            <Button transparent>
              <Icon name='heart' />
            </Button>
            <Button transparent>
              <Icon name='more' />
            </Button>
          </Right>
        </Header>
        <Content padder>
          <Card>
            <CardItem>
              <Body>
              <View style={{flex: 1, paddingTop:20}}>
                <FlatList
                  data={this.state.dataSource}
                  renderItem={({item}) => <Text>{item.name}</Text>}
                  keyExtractor={({id}, index) => id}
                />
        
              </View>
              </Body>
            </CardItem>
          </Card>
          <Button dark bordered
            onPress= {() => {Actions.pop(); }}>
            <Text>Goto Page 1</Text>
         </Button>
       </Content>
       <Footer>
          <FooterTab>
            <Button vertical>
              <Icon name="home" />
              <Text>Home</Text>
            </Button>
            <Button vertical>
              <Icon name="apps" />
              <Text>List</Text>
            </Button>
            <Button vertical active>
              <Icon active name="navigate" />
              <Text>Navigate</Text>
            </Button>
            <Button vertical>
              <Icon name="person" />
              <Text>Contact</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}