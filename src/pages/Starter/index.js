// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  Alert,
  Icon,
  AsyncStorage,
  NetInfo,
  ActivityIndicator,
  BackHandler,
  ImageBackground
} from 'react-native';
import {Button} from 'native-base';
import Axios from 'axios';

import { Actions } from 'react-native-router-flux';

export default class StarterPage extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
        <React.Fragment>
            <ImageBackground source={ require('./../../img/background.jpg')} style={{width: '100%', height: '100%'}}>
                <View style={styles.container}>
                    <View>
                        <Image style={styles.iconuser} source={require('./../../img/logo.png')}/>
                    </View>
                    <View>
                        <Text style = {styles.header}>
                            Welcome To This App
                        </Text>
                    </View>
                    <View>
                        {/* <Text style = {styles.paragraph}>
                            Stay up to date with every information of your employee.
                            Keep the track of every details of your employee 
                        </Text> */}
                    </View>
                    <View>
                        <Button info flats onPress = {()=>{Actions.login()}} style = {[styles.buttonContainer, styles.loginButton]}>
                            <Text style = {styles.loginText}>
                                Login
                            </Text>
                        </Button>
                    </View>
                </View>
            </ImageBackground>
        </React.Fragment>
        );
    }
}
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    //   backgroundColor: '#DCDCDC',
    },
    header:{
        color:'#fff',
        fontSize:25,
    },
    paragraph:{
        color:'#fff',
        fontSize:15,
        justifyContent:'center',
        marginTop:10,
    },
    inputContainer: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        // borderRadius:30,
        borderBottomWidth: 1,
        width:300,
        height:45,
        marginBottom:20,
        flexDirection: 'row',
        alignItems:'center'
    },
    inputs:{
        height:45,
        marginLeft:16,
        borderBottomColor: '#FFFFFF',
        flex:1,
    },
    inputIcon:{
      width:30,
      height:30,
      marginLeft:15,
      justifyContent: 'center'
    },
    iconuser:{
      width:200,
      height:100,
      marginLeft:15,
      marginBottom:20,
      justifyContent: 'center'
    },
    buttonContainer: {
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:250,
    },
    loginButton: {
      backgroundColor: "#00b5ec",
      borderRadius:50,
      marginTop:100,
    },
    loginText: {
      color: 'white',
    }
  });
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
   