import React, { Component } from 'react';
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
import {
  FlatList, ActivityIndicator, AsyncStorage, Animated, TouchableHighlight, InlineImage,
  TouchableOpacity, textSwap, Alert, ListView, StyleSheet, WebView, Text
} from 'react-native';
import {
  Container, Content,
  Card, Header, Body, Button, Title,
  CardItem, View, Left, Right,
  Thumbnail, Icon,
  Footer, FooterTab,
  SwipeRow,
  Accordion, List, ListItem, Badge, Item, Input, Picker
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import SearchInput, { createFilter } from 'react-native-search-filter';
import Collapsible from './../../components/collapsible';
import AppFooter from './../../components/appFooter';
// import Text from './../../components/appFont';
import Axios from 'axios';

export default class Days extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      auth_token: '',
      dataSource: '',
      search: '',
      page: 1,
      sorting: 'A-Z'
    }
  }

  static navigationOptions = { header: null };

  showaz = () => {
    var data = this.state.dataSource;
    data = data.sort((a, b) => a.name.localeCompare(b.name));
    this.setState({
      dataSource: data,
    })
  }
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------

  showza = () => {
    var data = this.state.dataSource;
    data = data.sort((a, b) => b.name.localeCompare(a.name));
    this.setState({
      dataSource: data,
    })
  }

  ChangeSorting = (value) => {
    var data = this.state.dataSource
    if (value == 'A-Z') {
      data = data.sort((a, b) => a.name.localeCompare(b.name));
      this.setState({
        dataSource: data,
        sorting: 'A-Z',
      })
    } else {
      data = data.sort((a, b) => b.name.localeCompare(a.name));
      this.setState({
        dataSource: data,
        sorting: 'Z-A',
      })
    }
  }
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  reachEnd = () => {
    console.log('end is reached');
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };

  updateSearch = search => {
    this.setState({ search });

  };
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  renderHeader = () => {
    return (
      <Item rounded style={styles.search}>
        <Icon style={{ color: '#f25a29' }} active name='search' />
        <Input placeholder='Search Here'
          onChangeText={(search) => this.updateSearch(search)}
        />
      </Item>
    )
  };

  getDataFromServer = () => {
    Axios.get('getdays').
      then(response => {
        var data = response.data;
        console.log(data);
        //   data = data.sort((a, b) => a.name.localeCompare(b.name));
        this.setState({
          dataSource: data,
          isLoading: false,
        }, function () {

        });
      })
      .catch(error => {
        console.log(error);
        console.log("axios does not work");
      })
  }

  componentDidMount() {
    this.getDataFromServer();
  }
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  render() {
    if (this.state.isLoading) {
      return (
        <View style={[styles.loaderContainer, styles.loaderHorizontal]}>
          <ActivityIndicator size="large" color="#f25a29" />
        </View>
      )
    }
    const data = this.state.dataSource;
    return (
      <Container>
        <Card>
          <CardItem header bordered style={styles.header}>
            <Text style={styles.headerfont}>Search by Days</Text>
          </CardItem>
          <CardItem style={styles.details}>
            <Body>
              <FlatList
                data={data}
                renderItem={({ item }) => <Text>{item}</Text>}
                keyExtractor={item => item}
                listKey={item => item}
              />
            </Body>
          </CardItem>
        </Card>
      </Container>
      // <Text>Days Listing</Text>
    );
  }
}
const styles = StyleSheet.create({
  image: {
    width: 80,
    height: 40,
  },
  btnlg: {
    height: 50,
    width: 100,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  search: {
    backgroundColor: '#d8d8d8',
  },
  picker: {
    fontSize: 5,
    borderWidth: 1,
    height: 50,
    width: 100
  },
  pickerItem: {
    fontSize: 5,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  loadeHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
});
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------