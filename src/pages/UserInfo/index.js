import React, { Component } from 'react';
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
import { FlatList, ActivityIndicator, StyleSheet} from 'react-native';
import { Container, View, Icon,Item, Input,Picker} from 'native-base';
import { createFilter } from 'react-native-search-filter';
import Collapsible from './../../components/collapsible';
import Axios from 'axios';


export default class userInfo extends Component {
  constructor(props){
    super(props);
    this.state ={ 
      isLoading: true,
      auth_token: '',
      dataSource: '',
      search:'',
      page:1,
      sorting:'A-Z',
      days:[],
      selectedDay:'',
      listRefreshing:false,
    }
  }

  static navigationOptions = { header: null };

  showaz=()=>{
    var data = this.state.dataSource;
    data = data.sort((a, b) => a.name.localeCompare(b.name));
    this.setState({
      dataSource:data,
    })
  }
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------

  showza=()=>{
    var data = this.state.dataSource;
    data = data.sort((a, b) => b.name.localeCompare(a.name));
    this.setState({
      dataSource:data,
    })
  }

  selectDay = (value) =>{
    console.log(value)
    this.setState({
      selectedDay:value,
      listRefreshing:true,
    })
     Axios.get('users?day='+value)
     .then(res=>{
       console.log(res);
        this.setState({
          dataSource:res.data,
          // listRefreshing:false,
        });
        this.ChangeSorting(this.state.sorting);
     })
     .catch(err=>{
        console.log(err);
        this.setState({
          listRefreshing:false,
        });
     })
  }

  listRefresh = () =>{
    this.setState({
      listRefreshing:true,
    });
    console.log("refersh worked");
    Axios.get('users?day='+this.state.selectedDay)
     .then(res=>{
       console.log(res);
        this.setState({
          dataSource:res.data,
          // listRefreshing:false,
        });
        this.ChangeSorting(this.state.sorting);
     })
     .catch(err=>{
        console.log(err);
        this.setState({
          listRefreshing:false,
        });
     })
  }
  

  ChangeSorting = (value) =>{
    var data = this.state.dataSource
    if(value == 'A-Z'){
      data = data.sort((a, b) => a.name.localeCompare(b.name));
      this.setState({
        dataSource:data,
        sorting:'A-Z',
        listRefreshing:false,
      })
    }else{
      data = data.sort((a, b) => b.name.localeCompare(a.name));
      this.setState({
        dataSource:data,
        sorting:'Z-A',
        listRefreshing:false,
      })
    }
  }
  
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  reachEnd = () =>{
    console.log('end is reached');
  }
  
  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };

  updateSearch = search => {
    this.setState({ search });
    
  };
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  renderHeader = () => {
    return (
      <Item rounded style = {styles.search}>
            <Icon style={{color:'#f25a29'}} active name='search' />
            <Input placeholder='Search Here'
            onChangeText={(search) => this.updateSearch(search)}
            />
      </Item>
    )
  };

  getDays = () =>{
    Axios.get('getdays')
    .then(res=>{
      console.log(res.data);
      this.setState({
        days:res.data,
        isLoading:false,
      });
    })
    .catch(err=>{
      console.log(err);
    });
  }

  getDataFromServer = ()=>{
    Axios.get('users').
    then(response=>{
      var data = response.data;
      data = data.sort((a, b) => a.name.localeCompare(b.name));
      this.setState({
        dataSource: data,
        // isLoading: false,
      }, function(){
        
      });
      this.getDays();
    })
    .catch(error=>{
      console.log(error);
      console.log("axios does not work");
    })
  }

  componentDidMount(){
    this.getDataFromServer();
  }
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  render(){
    if(this.state.isLoading){
      return(
        <View style={[styles.loaderContainer, styles.loaderHorizontal]}>
            <ActivityIndicator size="large" color="#f25a29" />
          </View>
      )
    }
    const filteredData = this.state.dataSource.filter(createFilter(this.state.search,['name']))
    return(
      <Container padder>
      <View style = {{padding:10}}>
        <this.renderHeader/>
        <View style={{height:50}}>
          <View style={{flex: 1, flexDirection: 'row', height:70}}>
            <View style={{flex:1}}>
              <Picker
                selectedValue={this.state.sorting}
                style={styles.picker}
                onValueChange={(itemValue) =>
                  this.ChangeSorting(itemValue)
                }>
                <Picker.Item style = {styles.pickerItem} label="A-Z" value="A-Z"/>
                <Picker.Item style = {styles.pickerItem} label="Z-A" value="Z-A"/>
              </Picker>
            </View>
            <View style={{flex:1}}>
              <Picker
                selectedValue={this.state.selectedDay}
                // style={styles.picker}
                onValueChange={(itemValue) =>
                  this.selectDay(itemValue)
                }>
                <Picker.Item style = {styles.pickerItem} label="Select Day" value=""/>
                {this.state.days.map((data,id)=>{
                  return(
                    <Picker.Item style = {styles.pickerItem} label={data} value={data} key={id}/>
                  )
                })}
              </Picker>
            </View>
          </View>
        </View>
        </View>
          <FlatList 
            data={filteredData}
            refreshing = {this.state.listRefreshing}
            renderItem={({ item }) =>(<Collapsible data={item}/>)}
            keyExtractor = {item => item.name}
            // ItemSeparatorComponent={this.renderSeparator}
            // ListHeaderComponent={this.renderHeader}
            onRefresh={()=>this.listRefresh()}
            showsVerticalScrollIndicator={true}
          />
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  image: {
    width:80,
    height:40, 
  },
  btnlg:{
    height:50, 
    width:100,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  search:{
    backgroundColor: '#d8d8d8',
  },
  picker:{
    fontSize:5,
    borderWidth:1,
    height: 50,
    width: 100,
    // backgroundColor:"red",
    borderRadius:20,
    borderColor:"black",
  },
  pickerItem:{
    fontSize:5,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  loadeHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10
  }
});
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------