// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  AsyncStorage,
  NetInfo,
  ActivityIndicator,
  BackHandler,
  ImageBackground,
} from 'react-native';
import Axios from 'axios';
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------

// import { Actions } from 'react-native-router-flux';
// import OfflineNotice from './../../components/offlineNotice';
import App from './../../App';
import RNRestart from 'react-native-restart';
import {Icon} from 'native-base';

export default class LoginView extends Component {
  static navigationOptions = {
    header: {
      visible: false,
    }
  };
  
  constructor(props) {
    super(props);
    this.state = {
      username   : '',
      password: '',
      access_token: '',
      isLoggedIn:false,
      dataLoading:false,
    }
  }
  componentWillMount(){
    BackHandler.removeEventListener('hardwareBackPress',()=>{Alert.alert('asfsdgdfgh')});
  }
  static navigationOptions = { header: null };
  
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  Login = async () => {
    console.log("login pressed");
    if(this.state.username=='' || this.state.password=='' )
    {
      Alert.alert('Alert!','Please enter your username and Password');
    }
    else{
      this.setState({
        dataLoading:true,
      });
    var fd=new FormData();
    fd.append('email',this.state.username)
    fd.append('password',this.state.password)
    Axios.post('http://aapsys.geniusitsolutions.com.au/api/login',fd)
    .then((res)=>{
      if(res.data.access_token){
      console.log(res.data.access_token);
        let token_data = {
          access_token: res.data.access_token,
          token_type: res.token_type,
        };
        AsyncStorage.setItem('UID123', JSON.stringify(token_data), () => {
          AsyncStorage.getItem('UID123', (err, result) => {
            result = JSON.parse(result);
            RNRestart.Restart();
          });
        });

        Axios.defaults.baseURL = 'http://aapsys.geniusitsolutions.com.au/api/';
        Axios.defaults.headers.common = {'Authorization': 'bearer'+res.data.access_token}
      }else{
        this.setState({
          dataLoading:false,
        });
        Alert.alert("","Invalid username or password");
      }
    })
    .catch(err=>{
      console.log(err);
      this.setState({
        dataLoading:false,
      });
      Alert.alert("","Unable to establish connection with the server");
     
      
    })
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------



    // fetch('https://my.geniusitsolutions.com.au/api/login', {
    //       method: 'post',
    //       body:fd,  
    //     }).then((response) => response.json())
    //     .then((res) => {
    //       let token_data = {
    //         access_token: res.access_token,
    //         token_type: res.token_type,
    //       };

    //       // You only need to define what will be added or updated
    //       AsyncStorage.setItem('UID123', JSON.stringify(token_data), () => {
    //           AsyncStorage.getItem('UID123', (err, result) => {
    //             result = JSON.parse(result);
    //             if(result.access_token){
    //               this.setState({ auth_token: result.access_token});
    //               Actions.loggedIn();
    //             // console.error(result.access_token);
    //             }else{
    //              Alert.alert("Invalid username or password");
    //             }
    //           });
    //       });
    //     // store.set('access_token', res.access_token )
    //     // this.setState({ auth_token: res.access_token });
    //  })
    //  .catch((error)=>{
    //    this.setState({
    //      dataLoading:false,
    //    });
    //    console.log(error);
    //    Alert.alert('!!!','Error in Connecting the server');
    //  })


    
    }
  }
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------

  showLoggingIn=()=>{
    if(this.state.dataLoading== true){
      return(
        <ActivityIndicator  color="#f25a29"/>
      )
    }else{
      return null;
    }
  }
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  render() {
    if(this.state.isLoggedIn == true){
      return(<App/>)
    }
    return (
      <ImageBackground source={ require('./../../img/background.jpg')} style={{width: '100%', height: '100%'}}>
      <View style={styles.container}>
      <View>
      <Image style={styles.iconuser}  source={ require('./../../icon/aapspng.png')}/>

      </View>
        <View style={styles.inputContainer}>
        <Icon name='ios-person' style={[{color: '#f25a29'},styles.inputIcon]}/>
          {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/> */}
          <TextInput style={styles.inputs}
              autoCapitalize = 'none'
              placeholder="Username"
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(username) => this.setState({username})}/>
        </View>
        
        <View style={styles.inputContainer}>
        <Icon name='ios-key' style={[{color: '#f25a29'},styles.inputIcon]}/>
          {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/> */}
          <TextInput style={styles.inputs}
              autoCapitalize = 'none'
              placeholder="Password"
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(password) => this.setState({password})}/>
        </View>

        <View>
          <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]}
              onPress= {this.Login.bind(this)}>   
                    <Text style={styles.loginText}>Login</Text>
          </TouchableHighlight>
          {this.showLoggingIn()}
        </View>
        {/* <TouchableHighlight style={styles.buttonContainer}>
            <Text style = {{color:"#fff"}}>Forgot your password?</Text>
        </TouchableHighlight> */}
      
      </View>
      </ImageBackground>
      
    );
  }
  
}
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#DCDCDC',
  },
  inputContainer: {
      borderRadius:30,
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      // borderRadius:30,
      // borderBottomWidth: 1,
      width:300,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      borderRadius:5,
      height:45,
      marginLeft:16,
      // borderBottomColor: '#FFFFFF',
      flex:1,
  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  iconuser:{
    width:200,
    height:100,
    marginLeft:15,
    marginBottom:20,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
  },
  loginButton: {
    backgroundColor: "#f25a29",
    borderRadius:20,
  },
  loginText: {
    color: 'white',
  }
});
 // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
 // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
 // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
 // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------