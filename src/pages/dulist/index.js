// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
import React, { Component,} from 'react';
import { Image,  StyleSheet,AsyncStorage,Alert,FlatList,Linking,View} from 'react-native';
import { createStackNavigator } from 'react-navigation';


import { Container, Header, Content, Card, CardItem, 
  Thumbnail, Text, Button, Icon,
   Left,Right, Body,Title,List,ListItem } from 'native-base';
import { Actions } from 'react-native-router-flux';
import AppFooter from '../../components/appFooter';


export default class user extends Component {
  constructor(props){
    super(props)
  }
  static navigationOptions = { header: null };
// logoutAlert = () => {
//   Alert.alert(
//     "Alert",
//     "Are you sure you want to logout ?",
//     [
//       { text: "Yes", onPress: () => this.logout() },
//       { text: "cancel", onPress: () => ("OK Pressed") }
//     ],
//     { cancelable: false }
//   );
// }
// componentDidMount = () =>{
//   console.log(this.props.data);
// } 

dateDiffInDays = (a,b) =>{
  var _MS_PER_DAY = 1000 * 60 * 60 * 24;
   // Discard the time and time-zone information.
   const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
   const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
 
   return Math.floor((utc2 - utc1) / _MS_PER_DAY);
}

Indicator = (data) => {
 
  if(data.expires == 1){
    var utc = new Date().toJSON().slice(0,10).replace(/-/g,'-');
    var expiryDate = new Date(data.expiry_date);
    var currentDate = new Date(utc);
    difference = this.dateDiffInDays(currentDate, expiryDate);
    // console.log(difference);
    if(difference <= 0){
      return (
        <Icon name='close-circle' style={{color: 'red'}}/>
      );
    }else if(difference < 30){
      return (
        <Icon name='ios-alert' style={{color: 'orange'}}/>
      );
    }else{
      return (
        <Icon name='checkmark-circle' style={{color: 'green'}}/>
      );
    }
  }else{
    return (
        <Icon name='checkmark-circle' style={{color: 'green'}}/>
    );
  }
};

showExpiryDate = (data) =>{
  if(data.expiry_date == null){
    return null;
  }else{
    return(
    <CardItem>
      <Text>Expiry Date : {data.expiry_date}</Text>                           
    </CardItem>
    )
  }
  
};
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
   Details = (array) =>{
    //  console.log(array);
          return(
            <React.Fragment>
                <Card>
                    <CardItem header bordered style = {styles.header}>
                        <Text style = {styles.headerfont}>Availability</Text>
                    </CardItem>
                    <CardItem  style= {styles.details}>
                    <Body>
                    <FlatList
                        data={array.availability}
                        renderItem={({item}) => <Text>{item}</Text>}
                        keyExtractor = {item =>item}
                        listKey = {item =>item}
                        />
                    </Body>
                    </CardItem>
                </Card>
                <Card>
                    <CardItem header bordered style = {styles.header}>
                        <Text style = {styles.headerfont}>Sites</Text>
                    </CardItem>
                    <CardItem  style= {styles.details}>
                    <Body>
                    <FlatList
                        data={array.sites}
                        renderItem={({item}) => <Text>{item.name}</Text>}
                        keyExtractor = {item =>item.name}
                        listKey = {item =>item.name}
                        />
                    </Body>
                    </CardItem>
                </Card>
                <Card>
                    <CardItem header bordered style = {styles.header}>
                        <Text style = {styles.headerfont}>Document</Text>
                    </CardItem>
                    <CardItem style= {styles.details}>
                        <Body>
                        <FlatList
                            data={array.docs}
                            style= {{width: '100%'}}
                            renderItem={({item}) =>
                            <Card>
                                <CardItem>
                                    <Body><Text>{item.file_name}</Text></Body>
                                    <Right><Text>{this.Indicator(item)}</Text></Right>
                                </CardItem>
                                {/* <CardItem>
                                        <Text>Expiry Date : {item.expiry_date}</Text>
                               
                                </CardItem> */}
                                {this.showExpiryDate(item)}
                            </Card>
                            }
                            keyExtractor = {item =>item.file_location}
                            listKey = {item =>item.file_location}
                            ItemSeparatorComponent={this.renderSeparator}
                            />
                        </Body>
                    </CardItem>
                </Card>
            </React.Fragment>
          );
      };
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------

logout = () => {
  AsyncStorage.clear().then(() => {
    Actions.login();
  }).catch(function(error) {
   alert.alert('Something went Wrong!!!');
  });
}
  render(){
    
    return(
      <React.Fragment>
        <View style={{height:50, borderBottomColor:'#b8cada', borderBottomWidth:0.5,}}>
          <View style={{flex: 1, flexDirection: 'row', height:70}}>
            <View style={{flex:2}}>
              <Button transparent onPress = {()=>{Actions.pop()}}>
                  <Icon name="arrow-back" />
              </Button>
            </View>
            <View style={{flex:10,alignItems:'center'}}>
              <Text style = {styles.name}>{this.props.data.name}</Text>
            </View>
            <View style={{flex:2}}>
              <Button rounded transparent success onPress={() =>{Linking.openURL(`tel:${this.props.data.phone_number}`)}}>
                <Icon active name="call" />
              </Button>
            </View>
          </View>
        </View>
      <Container>
        <Content padder>
         {/* <Image style={styles.userimg} source={{uri: 'https://icons-for-free.com/free-icons/png/512/1891016.png'}}/> */}
            {this.Details(this.props.data)}
        </Content>
      </Container>
      </React.Fragment>
    );
  }
}
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------

const styles = StyleSheet.create({
  userimg:{
    width:200,
    height:200,
    marginTop:20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  name:{
    fontSize:17,
    fontWeight:'bold',
    marginTop:10,
    color:'#51677d',
  },
  usern:{
    alignItems: 'center',
    justifyContent: 'center'
  },
  logoutbtn: {
    backgroundColor:'red',
  },
  collapsedDetails: {
    borderBottomWidth: 0.5,
    borderColor: '#d6d7da',
  },
  expandedDetails: {
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    backgroundColor: '#f3f8ff'
  },
  expandedColor:{
    backgroundColor: '#f3f8ff',
  },
  header :{
      backgroundColor: '#dbe0e4',
      height:30
  },
  headerfont :{
      fontWeight: 'bold',
  },
  red: {
    color: 'red',
  },
  swipabe: {
      paddingBottom: 0,
      paddingTop:0,
      paddingRight:2,
      borderBottomWidth:0,
  },
  highlightedNameText:{
      fontWeight:'bold',
      color:'#4875a5',
  }
});
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------

// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  