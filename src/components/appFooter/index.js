// THIS APP IS CREATED BY JOHN BAISHYA
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Image,
    Alert,
    AsyncStorage,
    FlatList
  } from 'react-native';
import {Button, Card, CardItem, Body, Left, Right, Footer, FooterTab, Icon, Badge} from 'native-base';
import { Actions } from 'react-native-router-flux';
import RNRestart from 'react-native-restart';
import AppHeader from './../appHeader';
import Axios from 'axios';
// THIS APP IS CREATED BY JOHN BAISHYA

// function updateState(text){
//   this.setState({currentPage:text})
// }

export default class AppFooter extends Component{
    constructor(props){
        super(props);
        this.state ={ 
          auth_token: '',
          dataSource: [],
          ActiveBtn: "list",
          currentPage: 'User List'
        }
      }
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
      Header = () =>{
        return(
          <Header>
            <Left>
              <Button transparent>
                <Icon name='list' />
              </Button>
            </Left>
            <Body>
              <Title>{this.state.currentPage}</Title>
            </Body>
            <Right >
              <Button vertical transparent> 
              <Thumbnail style={styles.image} square source={ require('../../img/logo.png')} />
              </Button>
            </Right>
          </Header>
          );
      }
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------

      // updateHeaderState(text) {
      //   updateState(text)
      // }

      componentDidMount(){
        AsyncStorage.getItem('UID123', (err, result) => {
          result = JSON.parse(result);
          this.setState({ 
            auth_token: result.access_token,
          });
        //   fetch('https://my.geniusitsolutions.com.au/api/users?token='+this.state.auth_token)
        //   .then((response) => response.json())
        //   .then((responseJson) => {
        //     this.setState({
        //       dataSource: responseJson,
        //     }, function(){
        //     //   console.error(this.state.dataSource.length);
        //     });
        // });
        Axios.get('users')
        .then((response)=>{
          // console.log(response.data);
          this.setState({
                  dataSource: response.data,
                }, function(){
                //   console.error(this.state.dataSource.length);
                });
        })
        .catch();
      })
      .catch((error) =>{
        console.error(error);
      });
  }
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
      logoutAlert = () => {
        Alert.alert(
          "Alert",
          "Are you sure you want to logout ?",
          [
            { text: "Yes", onPress: () => this.logout() },
            { text: "cancel", onPress: () => ("OK Pressed") }
          ],
          { cancelable: false }
        );
      
      
      
      }
      
      logout = () => {
        AsyncStorage.clear().then(() => {
          // CodePush.restartApp();
          RNRestart.Restart();
        }).catch(function(error) {
         alert.alert('Something went Wrong!!!');
        });
      }

      functionListbtn = () =>{
        this.setState({
          ActiveBtn:'list',
        });
        // this.updateHeaderState('User List');
        Actions.users();
      }
      functionSitesBtn = () =>{
        this.setState({
          ActiveBtn:"sites",
        });
        // this.updateHeaderState('Sites');
        Actions.allSites();
      }

      functionSearchBtn = () =>{
        this.setState({
          ActiveBtn:"sites",
        });
        // this.updateHeaderState('Sites');
        Actions.dsearch();
      }
      functionUserBtn = () =>{
        this.setState({
          ActiveBtn:"user",
        });
        // this.updateHeaderState('Sites');
        Actions.user();
      }
    render(){
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
      // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------  
      return(
            <Footer>
                <FooterTab>
                    <Button badge vertical active={this.state.ActiveBtn === 'list'} onPress = {() => {this.functionListbtn()}}>
                        <Badge Badge info>
                        <Text style={styles.footxt}>{this.state.dataSource.length}</Text>
                        </Badge>
                        <Icon name="list" />
                        <Text style={styles.footxt}>list</Text>
                    </Button>
                    <Button vertical active ={this.state.ActiveBtn === 'sites'} onPress = {() => {this.functionSitesBtn()}}>
                        <Icon active name="grid"/>
                        <Text style={styles.footxt}>sites</Text>
                    </Button>
                    {/* <Button vertical active ={this.state.ActiveBtn === 'search'} onPress = {() => {this.functionSearchBtn()}}>
                        <Icon active name="search"/>
                        <Text style={styles.footxt}>Search</Text>
                    </Button> */}
                    <Button vertical style={styles.logoutbtn}           
                       onPress= {() => {this.logoutAlert()}}>  
                        <Icon name="swap" />
                        <Text  style={styles.footxt}>Logout</Text>
                    </Button>
                    {/* <Button vertical active ={this.state.ActiveBtn === 'user'} onPress = {() => {this.functionUserBtn()}}>
                        <Icon name="person" />
                        <Text style={styles.footxt}>User Details</Text>
                    </Button> */}
                </FooterTab>
            </Footer>
        );
    }
}
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------

const styles = StyleSheet.create({
    footxt:{
        color: 'white',
    },
    footer:{
      backgroundColor:'#f25a29'
    }
   
  });

  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
  // THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------
// THIS APP IS CREATED BY JOHN BAISHYA------------------------------------------------