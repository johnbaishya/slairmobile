import React, { Component } from 'react';
import { Text as TextRN } from 'react-native';
import App from './../../App';

export default class Text extends Component {
  mainApp = new App;
  constructor(props){
    super(props);

    this.state = {
      fontSize: this.mainApp.updateState(),
    }
  }

  render() {
    console.log(this.state.fontSize);
    return (
      <TextRN style={{fontSize:this.state.fontSize}}>
        {this.props.children}
      </TextRN>
    )
  }
}